#ifndef DATASTORAGE_H
#define DATASTORAGE_H

#include <QObject>
#include <QVector>
#include <QXmlStreamWriter>
#include <QFile>

#include "gastempsensordata.h"

template<class T>
class DataStorage
{
public:
    DataStorage(QString name) : _name(name) {}
    //explicit DataStorage();
    T* operator[](size_t index);
    T* getEl(size_t index);
    DataStorage& operator+= (const T el);

//public slots:
    //void onDataUpdated(T data) { };
//signals:

    size_t size() const;
    size_t fullSize() const;

    //void writeIntoXml(QXmlStreamWriter &xmlWriter, size_t countOfEl) = 0;
    virtual void writeIntoFile(QFile &file, size_t countOfEl) = 0;
    virtual void write(size_t countofEl) = 0;

protected:
    QVector<T> _data;
    size_t _startData { 0 };
    size_t _size { 0 };
    size_t _maxSize { 100 };
    size_t _lastSavedElIndex { 0 };
    size_t _fullSize { 0 };
    size_t _sizeOfWritedEl;
    QString _name;
};

template<class T>
size_t DataStorage<T>::size() const
{
    return _size;
}

template<class T>
size_t DataStorage<T>::fullSize() const
{
    return _fullSize;
}

template<class T>
T* DataStorage<T>::operator[](size_t index)
{
    if(_size < index)
        return nullptr;
    return &_data[(_startData + index) % _maxSize];
}

template<class T>
DataStorage<T>& DataStorage<T>::operator+=(const T el)
{
    _fullSize++;
    if(_size != _maxSize)
    {
        _data.push_back(el);
        _size++;
        return *this;
    }
    //override el
    _data[_startData] = el;
    _startData++;
    if(_startData == _maxSize)
        _startData = 0;
}

template<class T>
T* DataStorage<T>::getEl(size_t index)
{
    return &_data[index];
}


class GasTempDataStorage : public QObject, public DataStorage<GasTempSensorData>
{
    Q_OBJECT
public:
    GasTempDataStorage():QObject(nullptr), DataStorage<GasTempSensorData>("gasTemp") {}

    void write(size_t countofEl)
    {
        QFile file(_name);
        file.open(QIODevice::Append | QIODevice::Text);
        writeIntoFile(file, countofEl);
        file.close();
    }

    void writeIntoFile(QFile &file, size_t countOfEl)
    {
        QString datastr;
        GasTempSensorData *data;
        for(size_t i = _lastSavedElIndex - _startData; i < _lastSavedElIndex + countOfEl; ++i)
        {
            data = getEl(i);
            datastr = data->time.toString("hh mm ss");
            datastr += ";";
            datastr += QString::number(data->temperature);
            datastr += ";";

            datastr += QString::number(data->humidity);
            datastr += ";";

            datastr += QString::number(data->eco);
            datastr += ";";

            datastr += QString::number(data->aqi);
            datastr += ";";

            datastr += QString::number(data->tvos);
            datastr += '\n';

            file.write(datastr.toUtf8());
        }
        _lastSavedElIndex += countOfEl;
        if (_lastSavedElIndex >= 100)
            _lastSavedElIndex -=100;
    }
    /*
    void writeIntoXml(QXmlStreamWriter &xmlWriter, size_t countOfEl)
    {
        xmlWriter.writeStartElement(_name);
        xmlWriter.writeTextElement("size", QString::number(fullSize());
        //size_t endEl = size() + countOfEl;
        for (size_t i = 0; i < _size + countOfEl; ++i)
        {
            xmlWriter.writeStartElement("measurement");
            xmlWriter.writeAttribute("id", );
        }
        xmlWriter.writeEndElement();
    }
*/

public slots:
    void onDataUpdated(GasTempSensorData *data)
    {
        qDebug()<<" GasTempDataStorage received data updated";
        this->operator +=(*data);
        write(1);
    }
};

#endif // DATASTORAGE_H
