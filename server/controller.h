#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include "sensor.h"
#include "clientmanager.h"
#include "sensorsmanager.h"
#include "datacollector.h"
#include "udpconnection.h"

class Controller : public QObject
{
    Q_OBJECT
public:
    explicit Controller(QObject *parent = nullptr);


public slots:
    void onRequestData();
    void onUpdateData(QByteArray);
    void onUserConnected();
    void onSensorConnected(SensorType sensorType, Sensor *sensor);
    void onDataReceived(char *buf, size_t size);

signals:
    void dataUpdated(QByteArray);

private:
    ClientManager *_clientManager;
    SensorsManager *_sensorManager;
    DataCollector *_dataCollector;
    bool _userConnected { false };
    UdpConnection *_udp;
};

#endif // CONTROLLER_H
