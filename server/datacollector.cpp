#include "datacollector.h"
#include "tempgassensor.h"

DataCollector::DataCollector()
{

}

void DataCollector::onSensorConnected(SensorType sensorType, Sensor *sensor)
{
    qDebug()<<"new sensor connected slot";
    if(sensorType == SensorType::TEMPGAS)
    {
        connect(dynamic_cast<TempGasSensor*>(sensor), &TempGasSensor::dataUpdated, &_gsData, &GasTempDataStorage::onDataUpdated);
    }
}
