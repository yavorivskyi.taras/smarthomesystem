#include <Arduino.h>
#include <ESP8266WiFi.h>

// put function declarations here:
int myFunction(int, int);

void setup() {
  // put your setup code here, to run once:
  //int result = myFunction(2, 3);
  pinMode(0, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(0, HIGH);
  delay(10000);
  digitalWrite(0, LOW);
  delay(10000);
}

// put function definitions here:
int myFunction(int x, int y) {
  return x + y;
}