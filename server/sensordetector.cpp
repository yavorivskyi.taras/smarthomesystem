#include "sensordetector.h"

#include "sensorCommonCommand.h"

SensorDetector::SensorDetector(QTcpSocket *socket, QObject *parent)
    : QObject{parent},
      _socket(socket)
{
    connect(socket, &QTcpSocket::readyRead, this, &SensorDetector::onReadReady);
}

void SensorDetector::requestSensorType()
{
    _socket->write(requestSensorTypeStr);
}

QTcpSocket *SensorDetector::socket() const
{
    return _socket;
}

void SensorDetector::onReadReady()
{
    QByteArray read = _socket->readAll();
    qDebug()<<"type "<<QString(read);
    emit typeDetected(this, QString(read));
}
