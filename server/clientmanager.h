#ifndef CLIENTMANAGER_H
#define CLIENTMANAGER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include "sensordatabase.h"


constexpr const char* getDataCommandName = "getSpecificData";


enum class Status
{
    NONE,
    CONNECTED,
    PENDING_DATA,
};

class ClientManager : public QObject
{
    Q_OBJECT
public:
    explicit ClientManager(QObject *parent = nullptr);
    void start();
    void sendData(QByteArray &data);

public slots:
    void onNewConnection();
    //void onTimer();
    void onReadyRead();
    void onDataUpdated(SensorDataBase* data);

signals:
    void requestData();
    void connected();

private:
    QTcpServer *_server { nullptr };
    QTcpSocket *_socket { nullptr };
    QHostAddress *_addr { nullptr };
    quint16 _port { 0 };
    Status _status;
};

#endif // CLIENTMANAGER_H
