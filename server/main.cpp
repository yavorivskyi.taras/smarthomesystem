#include <QCoreApplication>
#include "sensorsmanager.h"
#include "controller.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Controller controller;

    return a.exec();
}
