#include "clientmanager.h"
#include "datastorage.h"

#include <QFile>

ClientManager::ClientManager(QObject *parent)
    : QObject{parent},
    _status(Status::NONE)
{
    _server = new QTcpServer();
    //_addr = new QHostAddress("162.168.1.252");
    _addr = new QHostAddress("0.0.0.0");
    _port = 8888;
    connect(_server, &QTcpServer::newConnection, this, &ClientManager::onNewConnection);
}

void ClientManager::start()
{
    if (!_server->listen(*_addr, _port))
        qDebug()<<"listen error: "<<_server->errorString();
    else
        qDebug()<<"listen OK";
}

void ClientManager::sendData(QByteArray &data)
{
    qDebug()<<"data sended";

    _status = Status::CONNECTED;
    _socket->write(data);
}

void ClientManager::onNewConnection()
{
    _socket = _server->nextPendingConnection();
    if (!_socket)
    {
        qDebug()<<"connection is null";
        return;
    }
    qDebug()<<"connected";
    connect(_socket, &QTcpSocket::readyRead, this, &ClientManager::onReadyRead);
    emit connected();
}

void ClientManager::onReadyRead()
{
    QByteArray readData = _socket->readAll();
    QString data(readData);
    qDebug()<<"data requested";
    GasTempSensorData sensorData;
    if(data == "appRequestData")
    {
        if (_status == Status::PENDING_DATA)
            return;

        _status = Status::PENDING_DATA;
        emit requestData();
    }
    if(data == getDataCommandName)
    {
        GasTempDataStorage dataStorage;
        uint32_t timeMin = 20;
        QFile file("gasTemp");
        QTime now = QTime::currentTime();
        file.open(QIODevice::ReadWrite | QIODevice::Text);
        QByteArray dataToSend;
        while (!file.atEnd()) {
            QByteArray data = file.readLine();
            data.removeLast(); //remove \n
            qDebug()<<"data from file "<<data;
            QList<QByteArray> datas = data.split(';');
            QTime time;
            QString str = QString::fromUtf8(datas[0]);
            time = QTime::fromString(str, "hh mm ss");
            qDebug()<<"strtime "<<str<<"time "<<time;
            qDebug()<<"time diff "<<time.secsTo(now);

            if (time.secsTo(now) < timeMin * 60)
            {
                qDebug()<<"Not wery old data from file "<<data;
                dataToSend += "{";
                dataToSend += data;
                dataToSend += "}\n";
            }

            //if( (int)-1 < timeMin * 60)
            //    qDebug()<<"debug print";

            //if(data.size() == 0)
            //    break;
        }
        _socket->write(dataToSend);
    }
}

void ClientManager::onDataUpdated(SensorDataBase *data)
{
    _socket->write(data->toJson());
}
