#ifndef SENSORCOMMONCOMMAND_H
#define SENSORCOMMONCOMMAND_H


constexpr char *requestSensorTypeStr = "requestSensorType";
constexpr char *requestDataStr = "requestData";

//sensor types
constexpr char *tempGasSensorStr = "tempGas";


#endif // SENSORCOMMONCOMMAND_H
