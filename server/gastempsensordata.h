#ifndef GASTEMPSENSORDATA_H
#define GASTEMPSENSORDATA_H

#include "sensordatabase.h"

class GasTempSensorData : public SensorDataBase
{
public:
    QByteArray toJson()
    {
        QByteArray json;
        QString data = "{ "
                       "\"temperature\": " + QString::number(temperature) + ","
                                                         "\"humidity\": " + QString::number(humidity) + ","
                                                      "\"eco\": " + QString::number(eco) + ","
                                                 "\"aqi\": " + QString::number(aqi) + ","
                                                 "\"tvos\": " + QString::number(tvos) + ","
                                                  " }";
        json = data.toUtf8();
        return json;
    }
public:
    double temperature;
    double humidity;
    quint8 aqi;
    quint16 tvos;
    quint16 eco;
};

#endif // GASTEMPSENSORDATA_H
