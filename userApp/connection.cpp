#include "connection.h"

Connection::Connection(QWidget *parent)
    : QWidget{parent}
{
    _socket = new QTcpSocket;
    //_addr = new QHostAddress("162.168.1.252");
    _addr = new QHostAddress("127.0.0.1");
    _port = 8888;
    _timer = new QTimer(parent);
    connect(_socket, &QTcpSocket::connected, this, &Connection::onConnected);
    connect(_timer, &QTimer::timeout, this, &Connection::onTimer);
    _socket->connectToHost(*_addr, _port);
    _timer->setInterval(1000);
}

void Connection::requestData()
{
    _socket->write("appRequestData");
    _status = Status::DATA_REQUESTED;
}

void Connection::onConnected()
{
    qDebug()<<"connected";
    //_timer->start();
    _status = Status::CONNECTED;
    connect(_socket, &QTcpSocket::readyRead, this, &Connection::onRead);
    _socket->write(getDataCommandName);
}

void Connection::onRead()
{
    QByteArray readData = _socket->readAll();
    qDebug()<<"data received";
    qDebug()<<readData;

    emit dataUpdated(readData);
    //_timer->start();
}

void Connection::onTimer()
{
    requestData();
}
