#include "sensor.h"

#include "sensorCommonCommand.h"

Sensor::Sensor(QTcpSocket *socket): _socket(socket)
{
    connect(_socket, &QTcpSocket::readyRead, this, &Sensor::onReadyRead);
    connect(_socket, &QTcpSocket::aboutToClose, this, &Sensor::onAbortConnection);

    _timer = new QTimer();
    _timer->setInterval(6000);

    connect(_timer, &QTimer::timeout, this, &Sensor::requestData);
    _timer->start();
}

void Sensor::requestData()
{
    _socket->write(requestDataStr);
}

void Sensor::onAbortConnection()
{
    qDebug()<<"Abort connection";
}

void Sensor::onReadyRead()
{
    QByteArray byteArray = _socket->readAll();
    parseData(byteArray);

    _timer->start();
}
