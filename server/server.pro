QT -= gui
QT += network concurrent 3dcore

CONFIG += c++17 console
CONFIG -= app_bundle

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        clientmanager.cpp \
        controller.cpp \
        datacollector.cpp \
        main.cpp \
        sensor.cpp \
        sensordatabase.cpp \
        sensordetector.cpp \
        sensorsmanager.cpp \
        tempgassensor.cpp \
        udpconnection.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    clientmanager.h \
    config.h \
    controller.h \
    datacollector.h \
    datastorage.h \
    gastempsensordata.h \
    sensor.h \
    sensorCommonCommand.h \
    sensordatabase.h \
    sensordetector.h \
    sensorsmanager.h \
    tempgassensor.h \
    udpconnection.h
