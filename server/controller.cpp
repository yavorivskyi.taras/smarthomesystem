#include "controller.h"
#include <QRegularExpression>
#include "config.h"

Controller::Controller(QObject *parent)
    : QObject{parent}
{
    _sensorManager = new SensorsManager;
    _clientManager = new ClientManager;
    _dataCollector = new DataCollector;
    _udp = new UdpConnection(this);

    _sensorManager->start();
    _clientManager->start();
    connect(_clientManager, &ClientManager::requestData, this, &Controller::onRequestData);
    connect(_sensorManager, &SensorsManager::sensorConnected, _dataCollector, &DataCollector::onSensorConnected);
    connect(_sensorManager, &SensorsManager::sensorConnected, this, &Controller::onSensorConnected);
    connect(_clientManager, &ClientManager::connected, this, &Controller::onUserConnected);
    connect(_udp, &UdpConnection::dataReceived, this, &Controller::onDataReceived);

    //connect(_se)
}

void Controller::onRequestData()
{
    qDebug()<<"data requested on controller";
    Sensor *sensor = _sensorManager->getSpecificSensor();
    if(!sensor)
    {
        qDebug()<<"NoSensors";
        return;
    }
    QByteArray bytes;
    for(int i = 0; i < _dataCollector->_gsData.size(); ++i)
    {
        bytes += _dataCollector->_gsData[i]->toJson();
    }
    //bytes +=
    //= sensor->createJson();
    _clientManager->sendData(bytes);
}

void Controller::onUpdateData(QByteArray)
{

}

void Controller::onUserConnected()
{
    _userConnected = true;
    Sensor *sensor = _sensorManager->getSpecificSensor();
    connect(sensor, &Sensor::dataUpdated, _clientManager, &ClientManager::onDataUpdated);
}

void Controller::onSensorConnected(SensorType sensorType, Sensor *sensor)
{
    //if(_clientManager->)
    //connect()
}

void Controller::onDataReceived(char *buf, size_t size)
{
    qDebug()<<"On data received "<<buf;
    qDebug()<<"Regex\n"<<tempDataRegex;

    QRegularExpression regex(tempDataRegex);
    QRegularExpressionMatch match = regex.match(buf);
    if (match.hasMatch())
    {
        //qDebug()<<match.capturedTexts();
        //qDebug()<<"uid parced:"<<match.captured(1);
        //qDebug()<<"temperature parced:"<<match.captured(2);
        qDebug()<<"uid parced:"<<match.captured("uid");
        qDebug()<<"uid parced:"<<match.captured("type");
        qDebug()<<"uid parced:"<<match.captured("temperature");

    }
    delete buf;
}
