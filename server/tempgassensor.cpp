#include "tempgassensor.h"
#include <QJsonObject>
#include <QJsonDocument>
#include "gastempsensordata.h"

TempGasSensor::TempGasSensor(QTcpSocket *socket):Sensor(socket)
{
    //_data = new GasTempSensorData();
}

void TempGasSensor::parseData(QByteArray &byteArray)
{
    qDebug()<<"received Data : "<<byteArray;
    //_temperature = byteArray.toDouble();

    QJsonDocument jsonDocument = QJsonDocument::fromJson(byteArray);
    QJsonObject jsonObject = jsonDocument.object();
    qDebug()<<"pursce end";
    _data.temperature = jsonObject["temperature"].toDouble();
    _data.humidity = jsonObject["humidity"].toDouble();
    _data.eco = (uint16_t)jsonObject["eco"].toInt();
    _data.aqi = (uint8_t)jsonObject["aqi"].toInt();
    _data.tvos = (uint16_t)jsonObject["tvos"].toInt();
    _data.time = QTime::currentTime();
    emit dataUpdated(&_data);
    //qDebug()<<"parced obj:\ntemp:"<<data.temperature<<"\nhumidity:"<<_humidity<<"\neco:"<<_eco<<"\naqi"<<_aqi<<"\ntvos:"<<_tvos;
}

QByteArray TempGasSensor::createJson()
{
    QString data;

    //data += "{ temperature:" + QString::number(_temperature) + " }";

    QByteArray byteArra(data.toUtf8());
    return std::move(byteArra);
}
