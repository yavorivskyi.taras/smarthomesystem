#ifndef TEMPGASSENSOR_H
#define TEMPGASSENSOR_H
#include "sensor.h"
#include <QObject>
#include "gastempsensordata.h"

class TempGasSensor : public Sensor
{
    Q_OBJECT
public:
    TempGasSensor(QTcpSocket *socket);
    TempGasSensor() {}
    virtual void parseData(QByteArray &byteArray);
    virtual QByteArray createJson();

signals:
    void dataUpdated(GasTempSensorData*);

private:
    GasTempSensorData _data;
    /*
    double _temperature;
    double _humidity;
    quint8 _aqi;
    quint16 _tvos;
    quint16 _eco;
*/
};

#endif // TEMPGASSENSOR_H
