#ifndef SENSORDETECTOR_H
#define SENSORDETECTOR_H

#include <QObject>
#include <QTcpSocket>

class SensorDetector : public QObject
{
    Q_OBJECT
public:
    explicit SensorDetector(QTcpSocket *socket, QObject *parent = nullptr);
    void requestSensorType();
    QTcpSocket *socket() const;

public slots:
    void onReadReady();
signals:
    void typeDetected(const SensorDetector *sensorDetector, QString type);
private:
    QTcpSocket *_socket;
};

#endif // SENSORDETECTOR_H
