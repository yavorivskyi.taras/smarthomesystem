#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QLineSeries>
#include <QChart>
#include <QChartView>
#include <QRegularExpression>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    _connection = new Connection;
    connect(_connection, &Connection::dataUpdated, this, &MainWindow::onDataRevc);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onDataRevc(QByteArray byteArray)
{
    QRegularExpression regex("{(.*);(.*);(.*);(.*);(.*)}");
    for (const QRegularExpressionMatch &match  : regex.globalMatch(QString::fromUtf8(byteArray)))
    {
        qDebug()<<"print data";
        qDebug()<<match.captured(1);
    }
#if 0
    qDebug()<<"update data";

    QString data(byteArray);
    counter++;
    ui->data->setText(data);
    ui->data_2->setText(QString::number(counter));
    QLineSeries *graph = new QLineSeries(this);
    graph->append(0, 6);
    graph->append(2, 4);
    graph->append(3, 8);
    graph->append(7, 4);
    graph->append(10, 5);
    //*graph << QPointF(11, 1) << QPointF(13, 3) << QPointF(17, 6) << QPointF(18, 3) << QPointF(20, 2);
    auto chart = new QChart;
    chart->legend()->hide();
    chart->addSeries(graph);
    chart->createDefaultAxes();
    chart->setTitle("Simple Line Chart");
    QChartView *chartView = new QChartView(this);
    chartView->setChart(chart);
    chartView->setGeometry(10,10,300,300);
#endif
}
