#ifndef SENSOR_H
#define SENSOR_H
#include <QObject>
#include <QTcpSocket>
#include <QTimer>
#include <QByteArray>
#include "udpconnection.h"

#include "sensordatabase.h"

class Sensor : public QObject
{
    Q_OBJECT
public:
    Sensor(QTcpSocket *socket);
    Sensor() {}
    void requestData();
    void onAbortConnection();
    virtual void parseData(QByteArray &byteArray) = 0;
    virtual QByteArray createJson() = 0;

signals:
    void dataUpdated(SensorDataBase*);

private:
    void onReadyRead();

private:
    QTimer *_timer;
    QTcpSocket *_socket;
    //SensorDataBase *_data;
};

#endif // SENSOR_H
