#ifndef SENSORSMANAGER_H
#define SENSORSMANAGER_H
#include <QTcpServer>
#include <QVector>
#include <QObject>
#include <QTimer>
#include <QTcpSocket> //tomev

#include "sensor.h"
#include "sensordetector.h"

enum class SensorType
{
    TEMPGAS,
};

class SensorsManager : public QObject
{
    Q_OBJECT
public:
    SensorsManager();
    void start();
    //QString getConnectedSensorType(QTcpSocket *socket);
    //void buildSensorObj(QString sensorType);

    Sensor *getSpecificSensor();

public slots:
    void onNewConnection();
    void onTimer();
    void buildSensorObj(QString sensorType);
    void sensorTypeReceived(const SensorDetector *detector, QString sensorType);

    //void onReadyRead();
signals:
    void sensorConnected(SensorType sensorType, Sensor *sensor);


private:
    QTcpServer *_server { nullptr };
    QHostAddress *_addr { nullptr };
    quint16 _port { 0 };
    QVector<Sensor *> _sensors;
    QVector<SensorDetector *> _sensorsInDetect;
};

#endif // SENSORSMANAGER_H
