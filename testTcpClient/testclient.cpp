#include "testclient.h"

TestClient::TestClient(QObject *parent)
    : QObject{parent}
{
    _client = new QTcpSocket;
    //_addr = new QHostAddress("162.168.1.252");
    _addr = new QHostAddress("127.0.0.1");
    _port = 9999;
    connect(_client, &QTcpSocket::connected, this, &TestClient::onConnected);
    _client->connectToHost(*_addr, _port);
}

void TestClient::onConnected()
{
    _timer = new QTimer();
    _timer->setInterval(1000);
    connect(_timer, &QTimer::timeout, this, &TestClient::onTimer);
    connect(_client, &QTcpSocket::readyRead, this, &TestClient::onRead);
   // _timer->start();
}

void TestClient::onTimer()
{
    message+= "a";

   _client->write(message.toStdString().c_str(), message.length());
   if(_client->waitForReadyRead(10000))
   {
       QByteArray readData = _client->readAll();
       qDebug()<<"received :"<<QString(readData);
   }

    _timer->start();

}

void TestClient::onRead()
{
  qDebug()<<"on read";
  QByteArray readData = _client->readAll();
  qDebug()<<" read data"<< QString(readData);

  //_client->write("testtest");
  QString data = "{ "
                 "\"temperature\": " + QString::number(_temperature) + ","
                                                   "\"humidity\": " + QString::number(_humidity) + ","
                                                "\"eco\": " + QString::number(_eco) + ","
                                           "\"aqi\": " + QString::number(_aqi) + ","
                                           "\"tvos\": " + QString::number(_tvos) +
                                            " }";

  if (QString(readData) == requestSensorTypeStr)
     _client->write(tempGasSensorStr);
  if (QString(readData) == requestDataStr)
      _client->write(data.toUtf8());
  _temperature += 0.1;
  _humidity += 34.1;
  _eco++;
  _aqi++;
  _tvos++;
}
