#ifndef UDPCONNECTION_H
#define UDPCONNECTION_H

#include <QObject>
#include <QUdpSocket>

class UdpConnection : public QObject
{
    Q_OBJECT
public:
    explicit UdpConnection(QObject *parent = nullptr);

signals:
    void dataReceived(char *buf, size_t size);
private slots:
    void onReadyRead();

private:
    QUdpSocket *_socket { nullptr };
    QHostAddress *_addr { nullptr };
    quint16 _port { 0 };

};

#endif // UDPCONNECTION_H
