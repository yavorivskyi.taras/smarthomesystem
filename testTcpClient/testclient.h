#ifndef TESTCLIENT_H
#define TESTCLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QTimer>

constexpr char *requestSensorTypeStr = "requestSensorType";
constexpr char *requestDataStr = "requestData";

//sensor types
constexpr char *tempGasSensorStr = "tempGas";


class TestClient : public QObject
{
    Q_OBJECT
public:
    explicit TestClient(QObject *parent = nullptr);
    void onConnected();
    void onTimer();
    void onRead();
private:
    QTcpSocket *_client;
    QHostAddress *_addr;
    quint16 _port { 0 };
    QTimer *_timer;
    QString message;

    double _temperature { 10.1 };
    double _humidity { 1353.4 };
    quint8 _aqi { 3 };
    quint16 _tvos { 55 };
    quint16 _eco { 626 };
};

#endif // TESTCLIENT_H
