#include "udpconnection.h"
#include <QTime>


UdpConnection::UdpConnection(QObject *parent)
    : QObject{parent}
{
    _socket = new QUdpSocket();
    _addr = new QHostAddress("162.168.1.252");
    //_addr = new QHostAddress("127.0.0.1");
    _port = 7777;
    connect(_socket, &QUdpSocket::readyRead, this, &UdpConnection::onReadyRead);

    _socket->bind(QHostAddress::AnyIPv4, _port);
    qDebug()<<"UDP con open";

}

void UdpConnection::onReadyRead()
{
    char buf[300];
    while (_socket->hasPendingDatagrams())
    {
        QHostAddress addr;
        size_t size = _socket->readDatagram(buf, 300, &addr);
        char *dynBuf = new char[size + 1];
        memcpy(dynBuf, buf, size);
        dynBuf[size] = 0;
        qDebug()<<"UDP received data"<<buf << "from "<<addr<<QTime::currentTime();
        emit dataReceived(dynBuf, size);
        _socket->writeDatagram("received", addr, 6666);
    }
    //parseData(byteArray);
}
