#ifndef CONFIG_H
#define CONFIG_H

constexpr const char * gasTempDataRegex = "{\n"
                                          "type:(.*)\n" //sensor type
                                          "uid:(.*)\n" //sensor uid
                                          "temperature:(.*)\n"
                                          "humidity:(.*)\n"
                                          "eco:(.*)\n"
                                          "tvos:(.*)\n"
                                          "aqi:(.*)\n"
                                         "}";

#if 0
constexpr const char * tempDataRegex = "{"
                                         "type:(.*)" //sensor type
                                         "uid:(.*)" //sensor uid
                                         "temperature:(.*)"
                                         "}";
#endif

constexpr const char * tempDataRegex = "{\n"
                                       "type:(?<type>.*)\n"
                                       "uid:(?<uid>.*)\n"
                                       "temperature:(?<temperature>.*)\n"
                                       "measure time:(.*)\n"
                                       "prev time:(.*)\n"
                                       "inited:(.*)\n"
                                       "}";

#endif // CONFIG_H
