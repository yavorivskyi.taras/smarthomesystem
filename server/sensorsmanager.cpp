#include "sensorsmanager.h"
#include "tempgassensor.h"
#include "sensorCommonCommand.h"

#include <QtConcurrent>
#include <QThread>
#include <QCoreApplication>

SensorsManager::SensorsManager():
    QObject(nullptr)
{
    _server = new QTcpServer();
    //_addr = new QHostAddress("162.168.1.252");
    _addr = new QHostAddress("127.0.0.1");
    _port = 9999;
    connect(_server, &QTcpServer::newConnection, this, &SensorsManager::onNewConnection);
    _sensors.push_back(new TempGasSensor);
}

void SensorsManager::start()
{
    if (!_server->listen(*_addr, _port))
        qDebug()<<"listen error: "<<_server->errorString();
    else
        qDebug()<<"listen OK";
}

void getConnectedSensorType(QObject *main, QTcpSocket *socket)
{
    //socket->write()
    //QString sensorType = "empty";
    //qDebug()<<"requested Sensor type"<<    QCoreApplication::instance()->thread()<<"  "<<QThread::currentThread();
    //socket->write(requestSensorTypeStr);
    //QCoreApplication::instance()->thread();
    //if (socket->waitForReadyRead(10000))
    //{
    //    QByteArray readData = socket->readAll();
    //    sensorType = QString(readData);
    //    qDebug()<<"obtained Sensor type" << sensorType;

    //}

    //QMetaObject::invokeMethod(main, "buildSensorObj", Q_ARG(QString, sensorType));
}

void SensorsManager::buildSensorObj(QString sensorType)
{
    qDebug()<<"SensorType "<<sensorType;
    //return nullptr;
}

void SensorsManager::sensorTypeReceived(const SensorDetector *detector, QString sensorType)
{
    Sensor *sensor = nullptr;
    QTcpSocket *socket = detector->socket();

    if (sensorType == tempGasSensorStr)
    {
        sensor = new TempGasSensor(detector->socket());
    }

    if (sensor == nullptr)
    {
        socket->close();
        return;
    }

    _sensorsInDetect.removeOne(detector);

    qDebug()<<"size after remove "<< _sensorsInDetect.size();

    delete detector;

    _sensors.push_back(sensor);
    emit sensorConnected(SensorType::TEMPGAS, sensor);//XXX to change
}

Sensor *SensorsManager::getSpecificSensor()
{
    if(!_sensors.size())
        return nullptr;
    return _sensors[0];
}

void SensorsManager::onNewConnection()
{
    QTcpSocket *connection = _server->nextPendingConnection();
    if (!connection)
    {
        qDebug()<<"connection is null";
        return;
    }
    qDebug()<<"getted new connection";
    //QThread
    //QtConcurrent::run(getConnectedSensorType, this, connection);
    //Sensor *sensor = new TempGasSensor(con ection);
    //_sensors.push_back(sensor);
    SensorDetector *detector = new SensorDetector(connection);
    connect(detector, &SensorDetector::typeDetected, this, &SensorsManager::sensorTypeReceived);
    detector->requestSensorType();
    _sensorsInDetect.push_back(detector);
}

void SensorsManager::onTimer()
{
    //QString msg = "msgmsg";
    //connection->write(msg.toStdString().c_str(), msg.length());
    //qint64 size = connection->bytesAvailable();

    //if (size)
    //{
    //    QByteArray readData = connection->readAll();
    //    qDebug()<<"received :"<<QString(readData);
    //    //connection->write(readData);
    //}

    //_timer->start();
}
