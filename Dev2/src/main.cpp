#include <Arduino.h>
/*

constexpr int sensorPin = 3;
constexpr int ligthPin = 4;

int state = HIGH;
int sensorsValue = 0;



void setup()
{
  pinMode(sensorPin, INPUT);
  pinMode(ligthPin, OUTPUT);
  //Serial.begin(115200);
  //Serial.print("Hi");
}

void loop()
{


  digitalWrite(ligthPin, state);
  sensorsValue = analogRead(sensorPin);
  ////Serial.println(sensorsValue);
  if (sensorsValue < 2500)
    state = HIGH;
  else
    state = LOW;
  //delay(500);

}
*/
 
#include <Wire.h>
#include <WiFi.h>


enum {
  BMP280_REGISTER_DIG_T1 = 0x88,
  BMP280_REGISTER_DIG_T2 = 0x8A,
  BMP280_REGISTER_DIG_T3 = 0x8C,
  BMP280_REGISTER_DIG_P1 = 0x8E,
  BMP280_REGISTER_DIG_P2 = 0x90,
  BMP280_REGISTER_DIG_P3 = 0x92,
  BMP280_REGISTER_DIG_P4 = 0x94,
  BMP280_REGISTER_DIG_P5 = 0x96,
  BMP280_REGISTER_DIG_P6 = 0x98,
  BMP280_REGISTER_DIG_P7 = 0x9A,
  BMP280_REGISTER_DIG_P8 = 0x9C,
  BMP280_REGISTER_DIG_P9 = 0x9E,
  BMP280_REGISTER_CHIPID = 0xD0,
  BMP280_REGISTER_VERSION = 0xD1,
  BMP280_REGISTER_SOFTRESET = 0xE0,
  BMP280_REGISTER_CAL26 = 0xE1, /**< R calibration = 0xE1-0xF0 */
  BMP280_REGISTER_STATUS = 0xF3,
  BMP280_REGISTER_CONTROL = 0xF4,
  BMP280_REGISTER_CONFIG = 0xF5,
  BMP280_REGISTER_PRESSUREDATA = 0xF7,
  BMP280_REGISTER_TEMPDATA = 0xFA,
};


#define CCS811_MODE_IDLE                   (uint8_t)0
#define CCS811_MODE_1SEC                   (uint8_t)1
#define CCS811_MODE_10SEC                  (uint8_t)2
#define CCS811_MODE_60SEC                  (uint8_t)3
// Timings
#define CCS811_WAIT_AFTER_RESET_US     2000 // The CCS811 needs a wait after reset
#define CCS811_WAIT_AFTER_APPSTART_US  1000 // The CCS811 needs a wait after app start
#define CCS811_WAIT_AFTER_WAKE_US        50 // The CCS811 needs a wait after WAKE signal
#define CCS811_WAIT_AFTER_APPERASE_MS   500 // The CCS811 needs a wait after app erase (300ms from spec not enough)
#define CCS811_WAIT_AFTER_APPVERIFY_MS   70 // The CCS811 needs a wait after app verify
#define CCS811_WAIT_AFTER_APPDATA_MS     50 // The CCS811 needs a wait after writing app data


// Main interface =====================================================================================================


// CCS811 registers/mailboxes, all 1 byte except when stated otherwise
#define CCS811_STATUS           0x00
#define CCS811_MEAS_MODE        0x01
#define CCS811_ALG_RESULT_DATA  0x02 // up to 8 bytes
#define CCS811_RAW_DATA         0x03 // 2 bytes
#define CCS811_ENV_DATA         0x05 // 4 bytes
#define CCS811_THRESHOLDS       0x10 // 5 bytes
#define CCS811_BASELINE         0x11 // 2 bytes
#define CCS811_HW_ID            0x20
#define CCS811_HW_VERSION       0x21
#define CCS811_FW_BOOT_VERSION  0x23 // 2 bytes
#define CCS811_FW_APP_VERSION   0x24 // 2 bytes
#define CCS811_ERROR_ID         0xE0
#define CCS811_APP_ERASE        0xF1 // 4 bytes
#define CCS811_APP_DATA         0xF2 // 9 bytes
#define CCS811_APP_VERIFY       0xF3 // 0 bytes
#define CCS811_APP_START        0xF4 // 0 bytes
#define CCS811_SW_RESET         0xFF // 4 bytes

constexpr int wakePin = 0;

void connectToWiFi()
{
  WiFi.begin("borodaruda", "19881988");
  while (WiFi.status() != WL_CONNECTED) {
    //Serial.print('.');
    delay(1000);
  }
  //Serial.println(WiFi.localIP());
}

void i2cBusScan()
{
  //#if 0
  Wire.begin();
 
  byte error, address;
  int nDevices;
 
  Serial.println("Scanning...");
 
  nDevices = 0;
  for(address = 1; address < 127; address++ )
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
 
    if (error == 0)
    {
      Serial.print("I2C device found at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.print(address,HEX);
      Serial.println("  !");
 
      nDevices++;
    }
    else if (error==4)
    {
      Serial.print("Unknown error at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.println(address,HEX);
    }    
  }
  if (nDevices == 0)
    Serial.println("No I2C devices found\n");
  else
    Serial.println("done\n");
 
  delay(5000);           // wait 5 seconds for next scan
  //#endif
}

/*
bool i2cwrite(int regaddr, int count, const uint8_t * buf) {
  Wire.beginTransmission(_slaveaddr);              // START, SLAVEADDR
  Wire.write(regaddr);                             // Register address
  for( int i=0; i<count; i++) Wire.write(buf[i]);  // Write bytes
  int r= Wire.endTransmission(true);               // STOP
  return r==0;
}

bool i2cread(int regaddr, int count, uint8_t * buf) {
  Wire.beginTransmission(_slaveaddr);              // START, SLAVEADDR
  Wire.write(regaddr);                             // Register address
  int wres= Wire.endTransmission(false);           // Repeated START
  delayMicroseconds(_i2cdelay_us);                 // Wait
  int rres=Wire.requestFrom(_slaveaddr,count);     // From CCS811, read bytes, STOP
  for( int i=0; i<count; i++ ) buf[i]=Wire.read();
  return (wres==0) && (rres==count);
}
*/

#define SOFT_RESET 0xBA
#define INIT 0xBE
#define STATUS 0x71
#define START_MEASUREMEN 0xAC

#define AHT1X_INIT_CTRL_NORMAL_MODE       0x00  //normal mode on/off       bit[6:5], for AHT1x only
#define AHT1X_INIT_CTRL_CYCLE_MODE        0x20  //cycle mode on/off        bit[6:5], for AHT1x only
#define AHT1X_INIT_CTRL_CMD_MODE          0x40  //command mode  on/off     bit[6:5], for AHT1x only
#define AHTXX_INIT_CTRL_CAL_ON            0x08  //calibration coeff on/off bit[3]
#define AHTXX_INIT_CTRL_NOP               0x00  //NOP control, send after any "AHT1X_INIT_CTRL..."

bool write(int regaddr, int count, const uint8_t *buf);
bool read(int regaddr, int count, uint8_t *buf);
void initBME280();

WiFiClient client;
WiFiUDP Udp;


void setup()
{
  //Serial.begin(9600);
      Wire.begin();
  pinMode(6, OUTPUT);
  pinMode(0, OUTPUT);

  //while (!Serial);             // Leonardo: wait for //Serial monitor
  //Serial.println("\nI2C Scanner");
   // i2cBusScan();

/*
  pinMode(6, OUTPUT);

      digitalWrite(6, HIGH); 

  initBME280();
  digitalWrite(6, LOW); 
  sleep(30);
  */
#if 0
  //0x38
  //0x52
  uint8_t status = 0;

    uint8_t data[8] = {0, 0, 0, 0, 0, 0, 0, 0};

  //Serial.begin(9600);
  Wire.begin();

  Wire.beginTransmission(0x38);

  Wire.write(SOFT_RESET);

  if (Wire.endTransmission(true))
    //Serial.println("reset error");

  delay(20);

  //AHTXX_INIT_CTRL_CAL_ON | AHT1X_INIT_CTRL_NORMAL_MODE

  Wire.beginTransmission(0x38);
  Wire.write(INIT);
  Wire.write(AHTXX_INIT_CTRL_CAL_ON | AHT1X_INIT_CTRL_NORMAL_MODE);
  Wire.write(AHTXX_INIT_CTRL_NOP);
  if (Wire.endTransmission(true))
    //Serial.println("mode error");

  delay(10);
  Wire.beginTransmission(0x38);
  Wire.write(STATUS);
  if (Wire.endTransmission(true))
    //Serial.println("status error");
  Wire.requestFrom((uint8_t)0x38, (uint8_t)1);
  if (Wire.available() == 1) 
    status = Wire.read();
  //Serial.printf("status %02X\n", status);


  // ENS160

  //CSn ->high for i2c
  pinMode(0, OUTPUT);
  digitalWrite(0, HIGH);

  //INT - TODO - INPUT_PULLUP

    delay(10);



  Wire.beginTransmission(0x52);
  Wire.write(0x00);
  Wire.endTransmission();
  Wire.requestFrom(0x52, 2);
  
  Wire.readBytes(data, 2);
  //Serial.printf("PartId %02X %02X\n", data[0], data[1]);
  uint16_t part_id = data[0] | ((uint16_t)data[1] << 8);
  //Serial.println(part_id);
  //Wire.write(0xF0);

  




#endif
#if 0
  //Serial.println("WiFi init");

  WiFi.mode(WIFI_STA);

  WiFi.begin("borodaruda", "19881988");
 while (WiFi.status() != WL_CONNECTED) {
    //Serial.print('.');
    delay(1000);
  }
  //Serial.println(WiFi.localIP());
#endif 
 // client.connect("162.168.1.252", 9999);



  /*
  uint8_t sw_reset[] = {0x11,0xE5,0x72,0x8A};
  uint8_t app_start[] = {0, 0};
  uint8_t hw_id = 0;
  uint8_t hw_version = 0;
  uint8_t app_version[2];
  uint8_t status = 0;
  uint8_t err = 0;
      uint8_t mode = CCS811_MODE_1SEC;


  Wire.begin();
 
  //Serial.begin(9600);
  pinMode(wakePin, OUTPUT);
  digitalWrite(wakePin, LOW); 
  delayMicroseconds(CCS811_WAIT_AFTER_WAKE_US);

  Wire.beginTransmission(0x5A);
  Wire.write(0); 
  int wres = Wire.endTransmission(false);  
  if (wres == 0)
  {
    //Serial.println("devicePresent");
  }
  else
    goto err;

  if (write(CCS811_SW_RESET, 4, sw_reset))
  {
    //Serial.println("sw error");
    goto err;
  }

      if (read(CCS811_ERROR_ID, 1, &err))
  {
    //Serial.println("err error");
    goto err;
  }
    //Serial.printf("status %02X, error %02X\n", status, err);


  delayMicroseconds(CCS811_WAIT_AFTER_RESET_US);

  if (read(CCS811_HW_ID, 1, &hw_id))
  {
    //Serial.println("hw id error");
    goto err;
  }
        if (read(CCS811_ERROR_ID, 1, &err))
  {
    //Serial.println("err error");
    goto err;
  }
    //Serial.printf("status %02X, error %02X\n", status, err);

  
  if (read(CCS811_HW_VERSION, 1, &hw_version))
  {
    //Serial.println("hw ver error");
    goto err;
  }
        if (read(CCS811_ERROR_ID, 1, &err))
  {
    //Serial.println("err error");
    goto err;
  }
    //Serial.printf("status %02X, error %02X\n", status, err);


  if (read(CCS811_STATUS, 1, &status))
  {
    //Serial.println("status error");
    goto err;
  }
        if (read(CCS811_ERROR_ID, 1, &err))
  {
    //Serial.println("err error");
    goto err;
  }
    //Serial.printf("status %02X, error %02X\n", status, err);


  if (read(CCS811_FW_APP_VERSION, 2, app_version))
  {
    //Serial.println("app_version error");
    goto err;
  }
        if (read(CCS811_ERROR_ID, 1, &err))
  {
    //Serial.println("err error");
    goto err;
  }
    //Serial.printf("status %02X, error %02X\n", status, err);


  //Serial.printf("hw id %02X, %02X, status %02X, %02X%02X\n", hw_id, hw_version, status, app_version[0], app_version[1]);

  if (write(CCS811_APP_START, 0, app_start))
  {
    //Serial.println("app_version error");
    goto err;
  }
  delayMicroseconds(CCS811_WAIT_AFTER_APPSTART_US);
  status = 0;

    if (read(CCS811_ERROR_ID, 1, &err))
  {
    //Serial.println("err error");
    goto err;
  }

    // Check if the switch was successful
  if (read(CCS811_STATUS, 1, &status))
  {
    //Serial.println("status error");
    goto err;
  }


  //Serial.printf("status %02X, error %02X\n", status, err);

  for(int i = 0; i < 10; i++)
  {
  if (write(CCS811_APP_START, 0, app_start))
  {
    //Serial.println("app_version error");
    goto err;
  }
  delayMicroseconds(CCS811_WAIT_AFTER_APPSTART_US);
  status = 0;

    if (read(CCS811_ERROR_ID, 1, &err))
  {
    //Serial.println("err error");
    goto err;
  }

    // Check if the switch was successful
  if (read(CCS811_STATUS, 1, &status))
  {
    //Serial.println("status error");
    goto err;
  }


  //Serial.printf("status %02X, error %02X\n", status, err);
  }

  if (write(CCS811_MEAS_MODE, 1, &mode))
  {
    //Serial.println("mode");
    goto err;
  }
        if (read(CCS811_ERROR_ID, 1, &err))
  {
    //Serial.println("err error");
    goto err;
  }
    //Serial.printf("status %02X, error %02X\n", status, err);

  


  //while (!//Serial);             // Leonardo: wait for //Serial monitor
  ////Serial.println("\nI2C Scanner");

  err:
  //Serial.println("err error");
  //digitalWrite(wakePin, HIGH); 
  */
}

/*
/**************************************************************************/
/*
    readHumidity()

    Read relative humidity, in %

    NOTE:
    - relative humidity range........ 0%..100%
    - relative humidity resolution... 0.024%
    - relative humidity accuracy..... +-2%
    - response time............ 5..30sec
    - measurement with high frequency leads to heating of the
      sensor, must be > 2 seconds apart to keep self-heating below 0.1C
    - long-term exposure for 60 hours outside the normal range
      (humidity > 80%) can lead to a temporary drift of the
      signal +3%, sensor slowly returns to the calibrated state at normal
      operating conditions

    - sensors data structure:
      - {status, RH, RH, RH+T, T, T, CRC*}, *CRC for AHT2x only

    - normal operating range T -20C..+60C, RH 10%..80%
    - maximum operating rage T -40C..+80C, RH 0%..100%
*/
/**************************************************************************/
/*
float readHumidity(bool readAHT)
{
  if (readAHT == AHTXX_FORCE_READ_DATA) {_readMeasurement();} //force to read data via I2C & update "_rawData[]" buffer
  if (_status != AHTXX_NO_ERROR)        {return AHTXX_ERROR;} //no reason to continue, call "getStatus()" for error description

  uint32_t humidity   = _rawData[1];                          //20-bit raw humidity data
           humidity <<= 8;
           humidity  |= _rawData[2];
           humidity <<= 4;
           humidity  |= _rawData[3] >> 4;

  if (humidity > 0x100000) {humidity = 0x100000;}             //check if RH>100, no need to check for RH<0 since "humidity" is "uint"

  return ((float)humidity / 0x100000) * 100;
}
*/

/**************************************************************************/
/*
    readTemperature()

    Read temperature, in C 

    NOTE:
    - temperature range........ -40C..+85C
    - temperature resolution... 0.01C
    - temperature accuracy..... +-0.3C
    - response time............ 5..30sec
    - measurement with high frequency leads to heating of the
      sensor, must be > 2 seconds apart to keep self-heating below 0.1C

    - sensors data structure:
      - {status, RH, RH, RH+T, T, T, CRC*}, *CRC for AHT2x only
*/
/**************************************************************************/
/*
float readTemperature(bool readAHT)
{
  if (readAHT == AHTXX_FORCE_READ_DATA) {_readMeasurement();} //force to read data via I2C & update "_rawData[]" buffer
  if (_status != AHTXX_NO_ERROR)        {return AHTXX_ERROR;} //no reason to continue, call "getStatus()" for error description

  uint32_t temperature   = _rawData[3] & 0x0F;                //20-bit raw temperature data
           temperature <<= 8;
           temperature  |= _rawData[4];
           temperature <<= 8;
           temperature  |= _rawData[5];

  return ((float)temperature / 0x100000) * 200 - 50;
}


*/
RTC_DATA_ATTR uint8_t inited = 0;
RTC_DATA_ATTR uint16_t dig_T1; /**< dig_T1 cal register. */
RTC_DATA_ATTR int16_t dig_T2;  /**<  dig_T2 cal register. */
RTC_DATA_ATTR int16_t dig_T3;  /**< dig_T3 cal register. */
 
uint64_t current = 0;
uint64_t lastMeasureTime = 0;
RTC_DATA_ATTR uint64_t timeToSend = 0;

RTC_DATA_ATTR uint64_t measureCount = 0;
void loop()
{
  current = millis();
 // uint8_t config = 0b00100111;
 // write(BMP280_REGISTER_CONTROL, 1, &config);
 // Serial.printf("temp read start\n");

  digitalWrite(6, HIGH); 

  delay(100);
  initBME280();

  //sleep(10);
  uint8_t buf[3] = { 0 };
  buf[0] = 0xFF;
  while (buf[0] & 0b00001000)
  {
    delay(50);
    read(BMP280_REGISTER_STATUS, 1, buf);
    //Serial.printf("status %02x\n", buf[0]);
  }
  if (read(BMP280_REGISTER_TEMPDATA, 3, buf))
    //Serial.printf("read temperature error\n");
  
  digitalWrite(6, LOW); 
   //Serial.printf("temp read end\n");
  int32_t temp = buf[0] << 16 | buf [1] << 8 | buf[2];
  //Serial.printf("temp hex %08x\n", temp);

  int32_t var1, var2;
 //int32_t adc_T;
  temp >>= 4;
  
  var1 = ((((temp >> 3) - ((int32_t)dig_T1 << 1))) *
          ((int32_t)dig_T2)) >>
         11;

  var2 = (((((temp >> 4) - ((int32_t)dig_T1)) *
            ((temp >> 4) - ((int32_t)dig_T1))) >>
           12) *
          ((int32_t)dig_T3)) >>
         14;
  int32_t t_fine;
  t_fine = var1 + var2;
  float T = (t_fine * 5 + 128) >> 8;
  lastMeasureTime = millis() - current;

  measureCount++;
 // Serial.printf("temerature %d \n", t_fine);
 // Serial.printf("temerature %f\n", T);
 // Serial.printf("temerature %f\n", T / 100);
if (measureCount > 6)
{



 // config = 0b10110100;
  //write(BMP280_REGISTER_CONTROL, 1, &config);
  String string;
  string += "{\n";
  string += "type: temperature\n";
  string += "uid:2\n";
  string += "temperature:" + String(T / 100, 3) + "\n";
  string += "measure time:" + String(lastMeasureTime, DEC) + "\n";
  string += "prev time:" + String(timeToSend, DEC) + "\n";
  string += "inited:" + String(inited, DEC) + "\n";
  string += "measureCount:" + String(measureCount, DEC) + "\n";
  string += "}";
  //Serial.printf("send start\n");
  connectToWiFi();
  char bufferRead[20];
  uint16_t i = 0;
  while (1)
  {
  Udp.beginPacket("162.168.1.252", 7777);
  //Udp.beginPacket("192.168.160.184", 7777);
  Udp.write((uint8_t *)string.c_str(), string.length());
  Udp.endPacket();
  delay(100);
  //Serial.printf("send end\n");
  Udp.begin(6666);
  uint64_t timerTime = millis();
  uint16_t readed = 0;
  while(millis() - timerTime < 500)
  {


    int size = Udp.parsePacket();
    if (size)
    {
      Udp.read(bufferRead, 20);
      //Serial.printf("received %s\n", bufferRead);
      readed = 1;
      measureCount = 0;
      break;
    }
  }
    i++;
    if (readed || i == 5)
      break;
  }
  Udp.stop();
  WiFi.disconnect(true);
  timeToSend = millis() - current;
}
//sleep(5);
inited = 1;
//isSendNeeded = !isSendNeeded;
//Serial.printf("isSendNeeded %d\n", isSendNeeded);

#define uS_TO_S_FACTOR 1000000  /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP  10       /* Time ESP32 will go to sleep (in seconds) */
esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
  //Wire.beginTransmission(0x52);
  //Wire.write(0x10);
  //Wire.write(0x00);
  //Wire.en2dTransmission();
esp_deep_sleep_start();
  #if 0
    uint8_t data[8] = {0, 0, 0, 0, 0, 0, 0, 0};


    Wire.beginTransmission(0x52);
  Wire.write(0x10);
  Wire.write(0x01);
  Wire.endTransmission();
  delay(10);

  Wire.beginTransmission(0x52);
  Wire.write(0x10);
  Wire.endTransmission();
  Wire.requestFrom(0x52, 1);
  
  Wire.readBytes(data, 1);
  //Serial.printf("mode %02X \n", data[0]);


  Wire.beginTransmission(0x52);
  Wire.write(0x10);
  Wire.write(0x02);
  Wire.endTransmission();
  delay(10);

  Wire.beginTransmission(0x52);
  Wire.write(0x10);
  Wire.endTransmission();
  Wire.requestFrom(0x52, 1);
  
  Wire.readBytes(data, 1);
  //Serial.printf("mode %02X \n", data[0]);

  uint8_t _rawData[8] = {0, 0, 0, 0, 0, 0, 0, 0};

  Wire.beginTransmission(0x38);
  Wire.write(START_MEASUREMEN);
  Wire.write(0x33);
  Wire.write(0x00);
  if (Wire.endTransmission(true))
    //Serial.println("write error");

  delay(80);

  Wire.requestFrom(0x38, (uint8_t)1, (uint8_t)true);
  if (Wire.available() != 1)
  //Serial.println("state read error");
  _rawData[0] = Wire.read();
  ////Serial.printf("status %02X\n", _rawData[0]);

  Wire.requestFrom(0x38, 7, (uint8_t)true);
  ////Serial.printf("avaible %d\n", Wire.available());
  Wire.readBytes(_rawData, 7);

  //for(int i = 0; i < 8; i++)
  //  //Serial.printf("%02X\n", _rawData[i]);
  //Serial.println();

  uint32_t temperature  = _rawData[3] & 0x0F;
             temperature <<= 8;
             temperature  |= _rawData[4];
           temperature <<= 8;
           temperature  |= _rawData[5];
  float temperatureF = ((float)temperature / 0x100000) * 200 - 50;

  uint32_t humidity   = _rawData[1];                          //20-bit raw humidity data
           humidity <<= 8;
           humidity  |= _rawData[2];
           humidity <<= 4;
           humidity  |= _rawData[3] >> 4;

  if (humidity > 0x100000) {humidity = 0x100000;}             //check if RH>100, no need to check for RH<0 since "humidity" is "uint"

  float humidityF =  ((float)humidity / 0x100000) * 100;

  //Serial.printf("temp %f\n h %f\n\n", temperatureF, humidityF);
  
  Wire.beginTransmission(0x52);
  Wire.write(0x20);
  if (Wire.endTransmission(true))
    //Serial.println("write error");

  delay(80);

  Wire.requestFrom(0x52, (uint8_t)1, (uint8_t)true);
  uint8_t dataStatus =  Wire.read();
  //Serial.printf("data status %02X\n", dataStatus);
  uint16_t ecoInt;

  if (dataStatus & 0x02)
  {
     //Serial.println("readData");

       Wire.beginTransmission(0x52);
  Wire.write(0x21);
  if (Wire.endTransmission(true))
    //Serial.println("write error");

  delay(80);

  Wire.requestFrom(0x52, (uint8_t)1, (uint8_t)true);
  uint8_t aqi =  Wire.read();
  //Serial.printf("aqi %02X\n", aqi);

         Wire.beginTransmission(0x52);
  Wire.write(0x22);
  if (Wire.endTransmission(true))
    //Serial.println("write error");

  delay(80);

  Wire.requestFrom(0x52, (uint8_t)2, (uint8_t)true);
  uint8_t tvos[2];
  Wire.readBytes(tvos, 2);
  //Serial.printf("tvos %02X %02X\n", tvos[0], tvos[1]);


         Wire.beginTransmission(0x52);
  Wire.write(0x24);
  if (Wire.endTransmission(true))
    //Serial.println("write error");

  delay(80);

  Wire.requestFrom(0x52, (uint8_t)2, (uint8_t)true);
  uint8_t eco[2];
  Wire.readBytes(eco, 2);
  //Serial.printf("eco %02X %02X\n", eco[0], eco[1]);
  ecoInt = eco[0] << 8 || eco[1];

  }
  //String string ("hello message");
  String string;
  string += "{";
  string += "type: gasTemp";
  string += "uid:1";
  string += "temperature:" + String(temperatureF, 3);
  string += "humidity:" + String(humidityF, 3);
  string += "eco:" + String(0, DEC);
  string += "tvos:" + String(0, DEC);
  string += "aqi:" + String(aqi, DEC);
  string += "}";
#endif
 // Udp.beginPacket("162.168.1.252", 7777);

  //Udp.write((uint8_t *)string.c_str(), string.length());
//      Udp.endPacket();

//  sleep(10);
#if 0
#define uS_TO_S_FACTOR 1000000  /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP  15       /* Time ESP32 will go to sleep (in seconds) */

esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
  Wire.beginTransmission(0x52);
  Wire.write(0x10);
  Wire.write(0x00);
  Wire.endTransmission();

  esp_deep_sleep_start();
  #endif
  /*
    uint8_t buf[8] = {0, 0, 0, 0, 0, 0, 0, 0 };
    digitalWrite(wakePin, LOW); 
     if (read(CCS811_ALG_RESULT_DATA, 8, buf))
         //Serial.println("error");

    for(int i = 0; i < 8; i ++)
        //Serial.printf(" %02X", buf[i]);
    //Serial.println("---------------");
    //digitalWrite(wakePin, HIGH);


sleep(10);
*/
  /*
  byte error, address;
  int nDevices;
 
  //Serial.println("Scanning...");
 
  nDevices = 0;
  for(address = 1; address < 127; address++ )
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
 
    if (error == 0)
    {
      //Serial.print("I2C device found at address 0x");
      if (address<16)
        //Serial.print("0");
      //Serial.print(address,HEX);
      //Serial.println("  !");
 
      nDevices++;
    }
    else if (error==4)
    {
      //Serial.print("Unknown error at address 0x");
      if (address<16)
        //Serial.print("0");
      //Serial.println(address,HEX);
    }    
  }
  if (nDevices == 0)
    //Serial.println("No I2C devices found\n");
  else
    //Serial.println("done\n");
 
  delay(5000);           // wait 5 seconds for next scan
  */
}

bool write(int regaddr, int count, const uint8_t *buf)
{
  Wire.beginTransmission(0x76);

  Wire.write(regaddr);

  for (int i=0; i<count; i++)
    Wire.write(buf[i]);

  int wres = Wire.endTransmission(true);

  if (wres != 0)
  {
    //Serial.println("error");
  }

  return !!wres;
}

bool read(int regaddr, int count, uint8_t *buf)
{
  Wire.beginTransmission(0x76);
  Wire.write(regaddr);                             // Register address
  int wres = Wire.endTransmission(true);
  if (wres != 0)
  {
    //Serial.println("error");
    return false;
  }
  delayMicroseconds(50);                 // Wait
  //Wire.beginTransmission(0x5A);
  int rres=Wire.requestFrom(0x76, count);     // From CCS811, read bytes, STOP
  //for( int i=0; i<count; i++ ) buf[i]=Wire.read();
  //rres = Wire.endTransmission(false);
  Wire.readBytes(buf, rres);
  return !(rres==count);
}

#define BMP280_CHIPID (0x58) /**< Default chip ID. */

void initBME280()
{
  uint8_t buf[2];
  read(BMP280_REGISTER_CHIPID, 1, buf);

  uint8_t config = 0b00100111;
  write(BMP280_REGISTER_CONTROL, 1, &config);
  config = 0b11100000;
  write(BMP280_REGISTER_CONFIG, 1, &config);
  delay(20);

  if (!inited)
  {
  read(BMP280_REGISTER_DIG_T1, 2, buf);
  //Serial.printf("t1 %02X %02X\n", buf[0], buf[1]);
  dig_T1 = 0;
  dig_T2 = 0;
  dig_T3 = 0;
  dig_T1 = ((buf[1] << 8) | buf[0]);
  //dig_T1 = buf[1] << 8 ;
  //Serial.println(dig_T1, DEC);
  //Serial.printf("t1 %04X\n", dig_T1);


  read(BMP280_REGISTER_DIG_T2, 2, buf);
  dig_T2 = buf[1] << 8 | buf[0];
  //Serial.printf("t2 %02X %02X\n", buf[0], buf[1]);
  //Serial.println(dig_T2, DEC);

  read(BMP280_REGISTER_DIG_T3, 2, buf);
  dig_T3 = buf[1] << 8 | buf[0];
  //Serial.printf("t3 %02X %02X\n", buf[0], buf[1]);
  //Serial.println(dig_T3, DEC);
  delay(20);
  }

}
