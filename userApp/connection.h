#ifndef CONNECTION_H
#define CONNECTION_H

#include <QWidget>
#include <QTcpSocket>
#include <QTimer>

constexpr const char* getDataCommandName = "getSpecificData";


enum class Status
{
    NONE,
    CONNECTED,
    DATA_REQUESTED,
};

class Connection : public QWidget
{
    Q_OBJECT
public:
    explicit Connection(QWidget *parent = nullptr);

    void requestData();


public slots:
    void onConnected();
    void onTimer();
    void onRead();

signals:
    void dataUpdated(QByteArray);

private:
    QTcpSocket *_socket;
    QHostAddress *_addr;
    quint16 _port { 0 };
    Status _status { Status::NONE };
    QTimer *_timer;
};

#endif // CONNECTION_H
