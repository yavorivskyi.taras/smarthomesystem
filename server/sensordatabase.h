#ifndef SENSORDATABASE_H
#define SENSORDATABASE_H

//#include <QObject>
#include <QTime>

class SensorDataBase
{
public:
    SensorDataBase();
    virtual ~SensorDataBase(){}
    virtual QByteArray toJson() = 0;
    QTime time;
};

#endif // SENSORDATABASE_H
