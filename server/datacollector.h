#ifndef DATACOLLECTOR_H
#define DATACOLLECTOR_H

#include <QObject>
#include "datastorage.h"
#include "gastempsensordata.h"
#include "sensor.h"
#include "sensorsmanager.h"

class DataCollector : public QObject
{
    Q_OBJECT
public:
    DataCollector();

public slots:
    void onSensorConnected(SensorType sensorType, Sensor *sensor);

public:
//private:
    GasTempDataStorage _gsData;
};

#endif // DATACOLLECTOR_H
